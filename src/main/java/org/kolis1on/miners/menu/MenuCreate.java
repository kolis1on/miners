package org.kolis1on.miners.menu;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.kolis1on.miners.objects.Mine;
import org.kolis1on.miners.storage.Storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class MenuCreate {
    private static MenuCreate instance = null;
    private static Storage storage = Storage.getInstance();
    public static MenuCreate getInstance() {
        if(instance == null) {
            instance = new MenuCreate();
        }

        return instance;

    }
    public static Inventory mainInv;
    public static HashMap<Location, Inventory> mainInventories = new HashMap<Location, Inventory>();
    public static HashMap<Inventory, Inventory> subInventories = new HashMap<Inventory, Inventory>();

    public static Inventory mainMenu(JavaPlugin plugin, String name, Location loc) {

        int size = plugin.getConfig().getInt("menus.main.size");

        ConfigurationSection mainMenu = plugin.getConfig().getConfigurationSection("menus");
        for(String menu: mainMenu.getKeys(false)) {
            if (name.equalsIgnoreCase(menu)) {
                mainInv = Bukkit.createInventory(null, size, ChatColor.translateAlternateColorCodes('&',
                        plugin.getConfig().getString("menus." + name + ".display_name")));
            }
        }

        ConfigurationSection mainMenuItems = plugin.getConfig().getConfigurationSection("menus.main.items");
        for(String fill: mainMenuItems.getKeys(false)){

            List<Integer> slots = (List<Integer>) plugin.getConfig().getList("menus.main.items." + fill + ".slots");
            String material = plugin.getConfig().getString("menus.main.items." + fill + ".material");
            String display_nameItem = plugin.getConfig().getString("menus.main.items." + fill + ".display_name");
            List<String> lore = (List<String>) plugin.getConfig().getList("menus.main.items." + fill + ".lore");

            lore = (ArrayList<String>) lore.stream().map(p -> p.replace("&", "\u00a7").
                    replace("%time%", String.valueOf(storage.getTimeOne(loc)))).collect(Collectors.toList());

            ItemStack item = new ItemStack(Material.valueOf(material));
            ItemMeta itemMeta = item.getItemMeta();

            itemMeta.setDisplayName(display_nameItem);
            itemMeta.setLore(lore);

            item.setItemMeta(itemMeta);

            for(int slot: slots){
                mainInv.setItem(slot, item);
            }
    }

        return mainInv;
    }

    public static void createInv(Location location, JavaPlugin plugin){
        String name = storage.getName(location);
        Inventory mainInv = mainMenu(plugin,name , location);
        Inventory subInv = subMenu(plugin, name, storage.getLevel(location) + 1);
        mainInventories.put(location, mainInv);
        subInventories.put(mainInv, subInv);

        Mine mine = new Mine(storage.getLevel(location), location,storage.getTimeOne(location), mainInv, name);
        Mine.mines.put(location, mine);
    }

    public static Inventory subMenu(JavaPlugin plugin, String name, int level) {
        Inventory subInv = null;
        int size = plugin.getConfig().getInt("menus.submenu.size");


            subInv = Bukkit.createInventory(null, size, ChatColor.translateAlternateColorCodes('&',
                    plugin.getConfig().getString("menus.submenu.display_name")));



        ConfigurationSection subMenuItems = plugin.getConfig().getConfigurationSection("menus.submenu.items");

        for(String fill: subMenuItems.getKeys(false)){


                List<Integer> slots = (List<Integer>) plugin.getConfig().getList("menus.submenu.items." + fill + ".slots");
                String material = plugin.getConfig().getString("menus.submenu.items." + fill + ".material");
                String display_nameItem = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("menus.submenu.items." + fill + ".display_name"));
                List<String> lore = (List<String>) plugin.getConfig().getList("menus.submenu.items." + fill + ".lore");

                lore = (ArrayList<String>) lore.stream().map(p -> p.replace("&", "\u00a7")).collect(Collectors.toList());

                ItemStack item = new ItemStack(Material.valueOf(material));
                ItemMeta itemMeta = item.getItemMeta();

                itemMeta.setDisplayName(display_nameItem);
                itemMeta.setLore(lore);

                item.setItemMeta(itemMeta);

                for (int slot : slots) {
                    subInv.setItem(slot, item);
                }
                ConfigurationSection upgradeItems = plugin.getConfig().getConfigurationSection("menus." + name + ".items");
                for (String str : upgradeItems.getKeys(false)) {
                    if (!str.equalsIgnoreCase("0")) {
                        int slot = plugin.getConfig().getInt("menus." + name + ".items." + str + ".slot");
                        int lvl = Integer.valueOf(str);
                        if (lvl < level) {
                            ItemStack previous = new ItemStack(Material.valueOf(plugin.getConfig().getString("menus.submenu.previous.material")));
                            ItemMeta previousMeta = previous.getItemMeta();

                            List<String> previousLore = (List<String>) plugin.getConfig().getList("menus.submenu.previous.lore");
                            previousLore = previousLore.stream().map(p -> p.replace("&", "\u00a7")).collect(Collectors.toList());
                            previousMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("menus.submenu.previous.display_name")));
                            previousMeta.setLore(previousLore);

                            previous.setItemMeta(previousMeta);

                            subInv.setItem(slot, previous);

                        }
                        if (lvl > level) {
                            ItemStack next = new ItemStack(Material.valueOf(plugin.getConfig().getString("menus.submenu.next.material")));
                            ItemMeta nextMeta = next.getItemMeta();

                            List<String> nextLore = (List<String>) plugin.getConfig().getList("menus.submenu.next.lore");
                            nextLore = nextLore.stream().map(p -> p.replace("&", "\u00a7")).collect(Collectors.toList());
                            nextMeta.setDisplayName(plugin.getConfig().getString("menus.submenu.next.display_name"));
                            nextMeta.setLore(nextLore);

                            next.setItemMeta(nextMeta);

                            subInv.setItem(slot, next);

                        }
                        if (lvl == level) {
                            ItemStack current = new ItemStack(Material.valueOf(plugin.getConfig().getString("menus.submenu.current.material")));
                            ItemMeta currentMeta = current.getItemMeta();

                            List<String> currentLore = (List<String>) plugin.getConfig().getList("menus." + name + ".items." + str + ".lore");
                            currentLore = currentLore.stream().map(p -> p.replace("&", "\u00a7")).collect(Collectors.toList());
                            currentMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("menus.submenu.current.display_name")));
                            currentMeta.setLore(currentLore);

                            current.setItemMeta(currentMeta);

                            subInv.setItem(slot, current);


                        }
                    }
                }
        }
        return subInv;
    }

}
